package main.mesh;

import math.Vector4;

/**
 * @author David Gronlund
 */
public class CubeGenerator {

    public static Mesh generate(Vector4 location, float width, float height, float depth) {
        float[] vertices = new float[24 * 4];
        float[] textures = new float[24 * 4];
        float[] normals = new float[24 * 4];
        int[] indices = new int[36];

        for (int i = 0; i < 6; i++) {
        }

        return null;
    }

    private static void setVector(int offset, float[] array, float x, float y, float z, float w) {
        array[offset] = x;
        array[offset + 1] = y;
        array[offset + 2] = z;
        array[offset + 3] = w;
    }
}
