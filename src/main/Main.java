package main;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.asset.AssetFile;
import main.asset.ModelChunk;
import main.asset.TextureChunk;
import math.*;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.*;
import static org.lwjgl.opengl.GL11.*;

/**
 * @author David Gronlund
 */
public class Main {

    public static final Logger LOGGER = Logger.getLogger(Main.class.getName());
    private RenderableObject triangleBatch;
    private Shader lightShader;
    private Shader texShader;
    private Matrix4 modelMatrix = new Matrix4();
    private Matrix4 viewMatrix = new Matrix4();
    private Matrix4 projectionMatrix = GameMath.projectionMatrix(1, 10000, 45, 640 / 480);
    private Vector4 translation = new Vector4(0, 0, 0, 1);
    private float rotateX;
    private float rotateY;
    private float rotateZ;
    Texture tex;
    Texture texture1;

    static {
        try {
            LOGGER.addHandler(new FileHandler("errors.log", true));
        } catch (IOException ex) {
            LOGGER.log(Level.WARNING, ex.toString(), ex);
        }
    }

    public static void main(String[] args) {
        testMath();
        testIO();
        Main main = null;
        try {
            main = new Main();
            main.create();
            main.run();
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, ex.toString(), ex);
        } finally {
            if (main != null) {
                main.destroy();
            }
        }
    }

    public static void testMath() {
        Vector4 v1 = new Vector4(5, 1, 0);
        Vector4 v2 = new Vector4(2, 2, 0);
        Vector4 v3 = GameMath.projectVector(v1, v2);
        System.out.println(v3);
    }

    public static void testIO() {
    }

    public void create() throws LWJGLException {
        Display.setDisplayMode(new DisplayMode(640, 480));
        Display.setTitle("Test Platform rev.001 ©2012 101010 Software");
        Display.setVSyncEnabled(true);
        Display.create(new PixelFormat(), new ContextAttribs(3, 2).withProfileCompatibility(true));
        Keyboard.create();
        Mouse.setGrabbed(true);
        Mouse.create();
        initGL();
    }

    public void destroy() {
        Mouse.destroy();
        Keyboard.destroy();
        Display.destroy();
    }

    public void initGL() {
        glEnable(GL_BLEND);
        glEnable(GL_POLYGON_SMOOTH);
        glEnable(GL_DEPTH_TEST);
        glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glFrontFace(GL_CCW);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        long start = System.currentTimeMillis();
        AssetFile assets = AssetFile.load(new File("assets/test.ten"));
        ModelChunk model = (ModelChunk) assets.findChunk("carrier");
        System.out.println(System.currentTimeMillis() - start);
        TextureChunk tex1 = (TextureChunk) assets.findChunk("tex1");

        ByteBuffer buf = ByteBuffer.allocateDirect(tex1.getData().length);
        System.out.println(tex1.getData().length);
        buf.put(tex1.getData());
        buf.flip();

        texture1 = new Texture(buf, tex1.getWidth(), tex1.getHeight());
        texture1.bindTexture();
        texture1.useTexture();

        lightShader = new Shader(Shader.DEFAULT_LIGHT_VERTEX_PROGRAM, Shader.DEFAULT_LIGHT_FRAGMENT_PROGRAM);
        texShader = new Shader("assets/shaders/texture_shader");
        triangleBatch = new RenderableObject(GL_TRIANGLES, model.getVertices(), null, model.getNormals(), model.getTextureCoordinates(), model.getIndices());

        glViewport(0, 0, 640, 480);
    }

    public void run() {
        while (!Display.isCloseRequested() && !Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
            if (Keyboard.isKeyDown(Keyboard.KEY_1)) {
                Mouse.setGrabbed(true);
            } else if (Keyboard.isKeyDown(Keyboard.KEY_2)) {
                Mouse.setGrabbed(false);
            }
            if (Display.isVisible()) {
                render();
                inputFunc();
                Display.update();
            }
            Display.sync(60);
        }
    }

    public void update() {
    }

    public void render() {
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

        modelMatrix.setIdentity();
        viewMatrix.setIdentity();

        viewMatrix.translate(new Vector4(translation));
        viewMatrix.rotate(0, 1, 0, rotateY);
        viewMatrix.rotate(1, 0, 0, -rotateX);

        viewMatrix = GameMath.findCameraMatrix(viewMatrix, false);

        Matrix4 mvMatrix = new Matrix4(viewMatrix);
        Matrix4 pMatrix = new Matrix4(projectionMatrix);

        lightShader.useShader();
        lightShader.setUniformVector("vColor", new Vector4(1f, 1f, 1f, 1f));
        lightShader.setUniformMatrix("mvMatrix", mvMatrix);
        lightShader.setUniformMatrix("pMatrix", pMatrix);
        triangleBatch.draw(lightShader.getAttributes());

        Matrix4 mvpMatrix = new Matrix4(projectionMatrix).multiply(viewMatrix.translate(0, 2, 0));

        texture1.useTexture();
        texShader.useShader();
        texShader.setUniformMatrix("mvpMatrix", mvpMatrix);
        triangleBatch.draw(texShader.getAttributes());
    }

    public void inputFunc() {
        float speed = .05f;
        Vector4 trans = new Vector4();
        if (Mouse.isGrabbed()) {
            if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
                trans.z += 1 * speed;
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
                trans.z -= 1 * speed;
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
                trans.x -= 1 * speed;
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
                trans.x += 1 * speed;
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_Q)) {
                trans.y -= 1 * speed;
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_E)) {
                trans.y += 1 * speed;
            }
        }
        trans.rotate(new Vector4(0, 1, 0), -rotateY);
        translation.translate(trans);
        float mouseDeltaX = Mouse.getDX();
        float mouseDeltaY = Mouse.getDY();
        rotateY -= GameMath.toRadians(mouseDeltaX / 16);
        rotateY = GameMath.normalize(rotateY);
        rotateX -= GameMath.toRadians(mouseDeltaY / 16);
        rotateX = GameMath.normalize(rotateX);
    }
}
