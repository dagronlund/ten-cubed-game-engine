package main;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author David Gronlund
 */
public class Input implements Runnable {

    Thread input;
    private final AtomicBoolean running = new AtomicBoolean(true);

    public Input() {
    }

    public void stop() {
        running.set(false);
    }

    public void addMouseListener() {
    }

    public void addKeyListener() {
    }

    public void addDisplayListener() {
    }

    public void test() {
        input = new Thread(this, "input");
        input.start();
    }

    @Override
    public void run() {
        System.out.println("Threading Online");
        System.out.println(Thread.currentThread().getName());
        while (running.get()) {
            inputLoop();
            try {
                Thread.currentThread().sleep(1000);
            } catch (InterruptedException ex) {
            }
        }
    }

    private void inputLoop() {
    }
}
