package main;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Map;
import org.lwjgl.BufferUtils;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL11.*;

/**
 * @author David Gronlund
 */
public class RenderableObject {

    private int mode;
    private int elements;
    private int vertexBuffer = -1;
    private int colorBuffer = -1;
    private int textureBuffer = -1;
    private int indexBuffer = -1;
    private int normalBuffer = -1;

    public RenderableObject(int mode, float[] vertices, int[] indices) {
        this.mode = mode;
        elements = indices.length;

        FloatBuffer vertexStorage = BufferUtils.createFloatBuffer(vertices.length);
        vertexStorage.put(vertices);
        vertexStorage.flip();
        vertexBuffer = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        glBufferData(GL_ARRAY_BUFFER, vertexStorage, GL_STATIC_DRAW);

        IntBuffer indexStorage = BufferUtils.createIntBuffer(indices.length);
        indexStorage.put(indices);
        indexStorage.flip();
        indexBuffer = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexStorage, GL_STATIC_DRAW);

    }

    public RenderableObject(int mode, float[] vertices, float[] colors, float[] normals, float[] textures, int[] indices) {
        this.mode = mode;
        elements = indices.length;

        FloatBuffer vertexStorage = BufferUtils.createFloatBuffer(vertices.length);
        vertexStorage.put(vertices);
        vertexStorage.flip();
        vertexBuffer = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        glBufferData(GL_ARRAY_BUFFER, vertexStorage, GL_STATIC_DRAW);

        if (colors != null && colors.length > 0) {
            FloatBuffer colorStorage = BufferUtils.createFloatBuffer(colors.length);
            colorStorage.put(colors);
            colorStorage.flip();
            colorBuffer = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
            glBufferData(GL_ARRAY_BUFFER, colorStorage, GL_STATIC_DRAW);
        }

        if (normals != null && normals.length > 0) {
            FloatBuffer normalStorage = BufferUtils.createFloatBuffer(normals.length);
            normalStorage.put(normals);
            normalStorage.flip();
            normalBuffer = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
            glBufferData(GL_ARRAY_BUFFER, normalStorage, GL_STATIC_DRAW);
        }

        if (textures != null && textures.length > 0) {
            FloatBuffer textureStorage = BufferUtils.createFloatBuffer(textures.length);
            textureStorage.put(textures);
            textureStorage.flip();
            textureBuffer = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, textureBuffer);
            glBufferData(GL_ARRAY_BUFFER, textureStorage, GL_STATIC_DRAW);
        }

        IntBuffer indexStorage = BufferUtils.createIntBuffer(indices.length);
        indexStorage.put(indices);
        indexStorage.flip();
        indexBuffer = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexStorage, GL_STATIC_DRAW);
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public void draw(Map<String, Integer> attributes) {
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        int vertexLocation = attributes.get("inVertex");
        glVertexAttribPointer(vertexLocation, 4, GL_FLOAT, false, 0, 0);
        glEnableVertexAttribArray(vertexLocation);

        if (attributes.containsKey("inColor")) {
            glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
            int colorLocation = attributes.get("inColor");
            glVertexAttribPointer(colorLocation, 4, GL_FLOAT, false, 0, 0);
            glEnableVertexAttribArray(colorLocation);
        }

        if (attributes.containsKey("inNormal")) {
            glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
            int normalLocation = attributes.get("inNormal");
            glVertexAttribPointer(normalLocation, 4, GL_FLOAT, false, 0, 0);
            glEnableVertexAttribArray(normalLocation);
        }

        if (attributes.containsKey("inTexCoord0")) {
            glBindBuffer(GL_ARRAY_BUFFER, textureBuffer);
            int textureLocation = attributes.get("inTexCoord0");
            glVertexAttribPointer(textureLocation, 4, GL_FLOAT, false, 0, 0);
            glEnableVertexAttribArray(textureLocation);
        }

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
        glDrawElements(mode, elements, GL_UNSIGNED_INT, 0);
    }
//    public static RenderableObject createBox(float width, float height, float depth){
//        float[] vertices = new float[24*4];
//        float[] normals = new float[24*4];
//        float[] textures = new float[24*4];
//        
//        int[] indices = new int[24];
//        for(int i=0;i<indices.length;i++){
//            indices[i] = i;
//        }
//        
//        
//        
//        
//        
//        return new RenderableObject(GL_TRIANGLES,vertices, null,normals,textures,indices);
//    }
}
