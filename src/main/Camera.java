package main;

import math.*;
import org.lwjgl.input.Keyboard;

/**
 * @author David Gronlund
 */
public class Camera {

    private Matrix4 viewMatrix;
    private Matrix4 projectionMatrix;
    private Vector4 cameraPosition;
    private Vector4 lookVector;
    private Vector4 axisVector;
    private float rotationAngle;

    public Camera(float fov, float aspect, float near, float far) {
        lookVector = new Vector4(0, 0, 0);
        cameraPosition = new Vector4(0, 0, -25);
        viewMatrix = new Matrix4().setIdentity();
        projectionMatrix = GameMath.projectionMatrix(near, far, fov, aspect);
        axisVector = new Vector4(0, 0, 0);
        rotationAngle = 0;
    }

    public void updateInput() {

        if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
            Vector4 vector = new Vector4(lookVector).subtract(cameraPosition);
            vector.normalise();
            cameraPosition.subtract(vector);
            lookVector.subtract(vector);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
            Vector4 vector = new Vector4(lookVector).subtract(cameraPosition);
            vector.normalise();
            cameraPosition.add(vector);
            lookVector.add(vector);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
            Vector4 vector = new Vector4(1, 0, 0);
            vector.normalise();
            cameraPosition.add(vector);
            lookVector.add(vector);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            Vector4 vector = new Vector4(1, 0, 0);
            vector.normalise();
            cameraPosition.subtract(vector);
            lookVector.subtract(vector);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_I)) {
            axisVector = new Vector4(1, 0, 0);
            rotationAngle -= .005f;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_K)) {
            axisVector = new Vector4(1, 0, 0);
            rotationAngle += .005f;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_J)) {
            axisVector = new Vector4(0, 1, 0);
            rotationAngle += .005f;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_L)) {
            axisVector = new Vector4(0, 1, 0);
            rotationAngle -= .005f;
        }

        Matrix4 translateOrigin = new Matrix4();
        Matrix4 translateBack = new Matrix4();
        Matrix4 axisRotation = new Matrix4();
        Matrix4 finalMatrix = new Matrix4();

        translateOrigin.setToTranslate(-cameraPosition.x, -cameraPosition.y, -cameraPosition.z);
        translateBack.setToTranslate(cameraPosition.x, cameraPosition.y, cameraPosition.z);
        axisRotation.setToRotate(axisVector, rotationAngle);
        finalMatrix = translateBack.multiply(axisRotation).multiply(translateOrigin);

        lookVector.fromArray(GameMath.multiplyVector(finalMatrix.matrix, lookVector.toArray(), 4));

    }

    public Matrix4 getViewMatrix() {

        Matrix4 translation = new Matrix4().setIdentity();
        Matrix4 rotation = new Matrix4().setIdentity();

        System.out.println(lookVector);

        Vector4 viewZ = new Vector4(lookVector).subtract(cameraPosition);
        viewZ.normalise();
        Vector4 viewX = GameMath.crossProduct(viewZ, new Vector4(0, 1, 0));
        viewX.normalise();
        Vector4 viewY = GameMath.crossProduct(viewX, viewZ);
        viewY.normalise();

        rotation.set(0, 0, viewX.x);
        rotation.set(0, 1, viewX.y);
        rotation.set(0, 2, viewX.z);

        rotation.set(1, 0, viewY.x);
        rotation.set(1, 1, viewY.y);
        rotation.set(1, 2, viewY.z);

        rotation.set(2, 0, viewZ.x);
        rotation.set(2, 1, viewZ.y);
        rotation.set(2, 2, viewZ.z);

        System.out.println(rotation);

        //Maybe need to use
        //rotation.transpose();

        translation.setToTranslate(-cameraPosition.x, -cameraPosition.y, -cameraPosition.z);

        viewMatrix = new Matrix4(rotation).multiply(translation);

        return new Matrix4(viewMatrix);
    }

    public Matrix4 getProjectionMatrix() {
        return this.projectionMatrix;
    }
}
