package main;

import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;
import math.Matrix4;
import math.Vector3;
import math.Vector4;
import static org.lwjgl.opengl.GL20.*;

/**
 * @author David Gronlund
 */
public class Shader {

    private int program = -1;
    private Map<String, Integer> attributeLocations = new HashMap<String, Integer>();
    private Map<String, Integer> uniformLocations = new HashMap<String, Integer>();

    public Shader(String vertexSrc, String fragmentSrc) {

        int vertexShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShader, vertexSrc);
        glCompileShader(vertexShader);

        String vertexLog = glGetShaderInfoLog(vertexShader, 65536);
        if (vertexLog.length() != 0) {
            System.out.println("Vertex Shader Error Log:\n" + vertexLog);
        }

        int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, fragmentSrc);
        glCompileShader(fragmentShader);

        String fragmentLog = glGetShaderInfoLog(vertexShader, 65536);
        if (fragmentLog.length() != 0) {
            System.out.println("Fragment Shader Error Log:\n" + fragmentLog);
        }

        program = glCreateProgram();

        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);
        glLinkProgram(program);

        String programLog = glGetProgramInfoLog(program, 65536);
        if (programLog.length() != 0) {
            System.out.println("Program Error Log:\n" + programLog);
        }

        int numAttributes = glGetProgram(program, GL_ACTIVE_ATTRIBUTES);
        int maxAttributeLength = glGetProgram(program, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH);
        for (int i = 0; i < numAttributes; i++) {
            String name = glGetActiveAttrib(program, i, maxAttributeLength);
            int location = glGetAttribLocation(program, name);
            attributeLocations.put(name, location);
        }

        int numUniforms = glGetProgram(program, GL_ACTIVE_UNIFORMS);
        int maxUniformLength = glGetProgram(program, GL_ACTIVE_UNIFORM_MAX_LENGTH);
        for (int i = 0; i < numUniforms; i++) {
            String name = glGetActiveUniform(program, i, maxUniformLength);
            int location = glGetUniformLocation(program, name);
            uniformLocations.put(name, location);
        }

    }

    public Shader(String shaderFileURL) {
        this(Utils.loadFile(shaderFileURL + ".vert"), Utils.loadFile(shaderFileURL + ".frag"));
    }

    public void useShader() {
        glUseProgram(program);
    }

    public int getProgram() {
        return program;
    }

    public void setUniformInteger(String id, int num) {
        int location = uniformLocations.get(id);
        glUniform1i(location, num);
    }

    public void setUniformVector(String id, Vector4 vec) {
        int location = uniformLocations.get(id);
        glUniform4f(location, vec.x, vec.y, vec.z, vec.w);
    }

    public void setUniformVector(String id, Vector3 vec) {
        int location = uniformLocations.get(id);
        glUniform3f(location, vec.x, vec.y, vec.z);
    }

    public void setUniformMatrix(String id, Matrix4 mat) {
        int location = uniformLocations.get(id);
        FloatBuffer temp = mat.toBuffer();
        temp.flip();
        glUniformMatrix4(location, false, temp);
    }

    public void setUniformMatrix(String id, FloatBuffer mat) {
        int location = uniformLocations.get(id);
        glUniformMatrix4(location, false, mat);
    }

    public Map getAttributes() {
        return this.attributeLocations;
    }

    public Map getUniforms() {
        return this.uniformLocations;
    }
    //// GLT_SHADER_DEFAULT_LIGHT
    //// Simple diffuse, directional, and vertex based light
    public static final String DEFAULT_LIGHT_VERTEX_PROGRAM =
            "uniform mat4 mvMatrix;"
            + "uniform mat4 pMatrix;"
            + "uniform vec4 vColor;"
            + "varying vec4 outFragColor;"
            + "attribute vec4 inVertex;"
            + "attribute vec4 inNormal;"
            + "void main(void) { "
            + "    vec3 newNorm = vec3(inNormal);"
            + "    mat3 mNormalMatrix;"
            + "    mNormalMatrix[0] = mvMatrix[0].xyz;"
            + "    mNormalMatrix[1] = mvMatrix[1].xyz;"
            + "    mNormalMatrix[2] = mvMatrix[2].xyz;"
            + "    vec3 vNorm = normalize(mNormalMatrix * newNorm);"
            + "    vec3 vLightDir = vec3(0.0, 0.0, 1.0); "
            + "    float fDot = max(0.0, dot(vNorm, vLightDir))/2.0 + 0.5; "
            + "    outFragColor.rgb = vColor.rgb * fDot;"
            + "    outFragColor.a = vColor.a;"
            + "    mat4 mvpMatrix;"
            + "    mvpMatrix = pMatrix * mvMatrix;"
            + "    gl_Position = mvpMatrix * inVertex; "
            + "}";
    public static final String DEFAULT_LIGHT_FRAGMENT_PROGRAM =
            "varying vec4 outFragColor; "
            + "void main(void) { "
            + " gl_FragColor = outFragColor; "
            + "}";
    ////GLT_SHADER_POINT_LIGHT_DIFF
    //// Point light, diffuse lighting only
    public static final String POINT_LIGHT_DIFF_VERTEX_PROGRAM =
            "uniform mat4 mvMatrix;"
            + "uniform mat4 pMatrix;"
            + "uniform vec3 vLightPos;"
            + "uniform vec4 inColor;"
            + "attribute vec4 inVertex;"
            + "attribute vec3 inNormal;"
            + "varying vec4 vFragColor;"
            + "void main(void) { "
            + "   mat3 mNormalMatrix;"
            + "   mNormalMatrix[0] = normalize(mvMatrix[0].xyz);"
            + "   mNormalMatrix[1] = normalize(mvMatrix[1].xyz);"
            + "   mNormalMatrix[2] = normalize(mvMatrix[2].xyz);"
            + "   vec3 vNorm = normalize(mNormalMatrix * inNormal);"
            + "   vec4 ecPosition;"
            + "   vec3 ecPosition3;"
            + "   ecPosition = mvMatrix * inVertex;"
            + "   ecPosition3 = ecPosition.xyz /ecPosition.w;"
            + "   vec3 vLightDir = normalize(vLightPos - ecPosition3);"
            + "   float fDot = max(0.0, dot(vNorm, vLightDir)); "
            + "   vFragColor.rgb = inColor.rgb * fDot;"
            + "   vFragColor.a = inColor.a;"
            + "   mat4 mvpMatrix;"
            + "   mvpMatrix = pMatrix * mvMatrix;"
            + "   gl_Position = mvpMatrix * inVertex; "
            + "}";
    public static final String POINT_LIGHT_DIFF_FRAGMENT_PROGRAM =
            "varying vec4 vFragColor; "
            + "void main(void) { "
            + " gl_FragColor = vFragColor; "
            + "}";
    ////GLT_SHADER_TEXTURE_REPLACE
    //// Just put the texture on the polygons
    public static final String TEXTURE_REPLACE_VERTEX_PROGRAM =
            "uniform mat4 mvpMatrix;"
            + "attribute vec4 inVertex;"
            + "attribute vec4 inTexCoord0;"
            + "varying vec4 vTex;"
            + "void main(void) "
            + "{ vTex = inTexCoord0;"
            + " gl_Position = mvpMatrix * inVertex; "
            + "}";
    public static final String TEXTURE_REPLACE_FRAGMENT_PROGRAM =
            "varying vec4 vTex;"
            + "uniform sampler2D textureUnit0;"
            + "void main(void) "
            + "{ gl_FragColor = vec4(vTex.x,vTex.y,1,1); "
            + "}";
    //// Just put the texture on the polygons
    public static final String TEXTURE_RECT_REPLACE_VERTEX_PROGRAM =
            "uniform mat4 mvpMatrix;"
            + "attribute vec4 vVertex;"
            + "attribute vec2 vTexCoord0;"
            + "varying vec2 vTex;"
            + "void main(void) "
            + "{ vTex = vTexCoord0;"
            + " gl_Position = mvpMatrix * vVertex; "
            + "}";
    public static final String TEXTURE_RECT_REPLACE_FRAGMENT_PROGRAM =
            "varying vec2 vTex;"
            + "uniform sampler2DRect textureUnit0;"
            + "void main(void) "
            + "{ gl_FragColor = texture2DRect(textureUnit0, vTex); "
            + "}";
    ////GLT_SHADER_TEXTURE_MODULATE
    //// Just put the texture on the polygons, but multiply by the color (as a unifomr)
    public static final String TEXTURE_MODULATE_VERTEX_PROGRAM =
            "uniform mat4 mvpMatrix;"
            + "attribute vec4 vVertex;"
            + "attribute vec2 vTexCoord0;"
            + "varying vec2 vTex;"
            + "void main(void) "
            + "{ vTex = vTexCoord0;"
            + " gl_Position = mvpMatrix * vVertex; "
            + "}";
    public static final String TEXTURE_MODULATE_FRAGMENT_PROGRAM =
            "varying vec2 vTex;"
            + "uniform sampler2D textureUnit0;"
            + "uniform vec4 vColor;"
            + "void main(void) "
            + "{ gl_FragColor = vColor * texture2D(textureUnit0, vTex); "
            + "}";
    //GLT_SHADER_TEXTURE_POINT_LIGHT_DIFF
    // Point light (Diffuse only), with texture (modulated)
    public static final String TEXTURE_POINT_LIGHT_DIFF_VERTEX_PROGRAM =
            "uniform mat4 mvMatrix;"
            + "uniform mat4 pMatrix;"
            + "uniform vec3 vLightPos;"
            + "uniform vec4 vColor;"
            + "attribute vec4 vVertex;"
            + "attribute vec3 vNormal;"
            + "varying vec4 vFragColor;"
            + "attribute vec2 vTexCoord0;"
            + "varying vec2 vTex;"
            + "void main(void) { "
            + " mat3 mNormalMatrix;"
            + " mNormalMatrix[0] = normalize(mvMatrix[0].xyz);"
            + " mNormalMatrix[1] = normalize(mvMatrix[1].xyz);"
            + " mNormalMatrix[2] = normalize(mvMatrix[2].xyz);"
            + " vec3 vNorm = normalize(mNormalMatrix * vNormal);"
            + " vec4 ecPosition;"
            + " vec3 ecPosition3;"
            + " ecPosition = mvMatrix * vVertex;"
            + " ecPosition3 = ecPosition.xyz /ecPosition.w;"
            + " vec3 vLightDir = normalize(vLightPos - ecPosition3);"
            + " float fDot = max(0.0, dot(vNorm, vLightDir)); "
            + " vFragColor.rgb = vColor.rgb * fDot;"
            + " vFragColor.a = vColor.a;"
            + " vTex = vTexCoord0;"
            + " mat4 mvpMatrix;"
            + " mvpMatrix = pMatrix * mvMatrix;"
            + " gl_Position = mvpMatrix * vVertex; "
            + "}";
    public static final String TEXTURE_POINT_LIGHT_DIFF_FRAGMENT_PROGRAM =
            "varying vec4 vFragColor;"
            + "varying vec2 vTex;"
            + "uniform sampler2D textureUnit0;"
            + "void main(void) { "
            + " gl_FragColor = vFragColor * texture2D(textureUnit0, vTex);"
            + "}";
}
