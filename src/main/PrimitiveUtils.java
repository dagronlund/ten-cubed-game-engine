package main;

/**
 * @author David Gronlund
 */
public class PrimitiveUtils {

    public static int unsign(byte value) {
        return value & 0xff;
    }

    public static byte[] readBytes(int data) {
        return new byte[]{
                    (byte) (data >>> 24),
                    (byte) (data >>> 16),
                    (byte) (data >>> 8),
                    (byte) (data)
                };
    }

    public static int fromBytes(byte[] data) {
        return (0xff & data[0]) << 24 | (0xff & data[1]) << 16 | (0xff & data[2]) << 8 | (0xff & data[3]) << 0;
    }

    public static boolean[] readBits(byte data) {
        boolean[] result = new boolean[8];
        byte mask = 0x01;
        for (int i = 0; i < 8; i++) {
            result[i] = (data & mask) != 0;
            mask <<= 1;
        }
        return result;
    }

    public static byte fromBits(boolean[] data) {
        byte result = 0;
        for (int i = 0; i < 8; i++) {
            result = (byte) (result | ((data[i] ? 1 : 0) << 7 - i));
        }
        return result;
    }
}
