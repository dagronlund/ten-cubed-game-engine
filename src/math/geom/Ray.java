package math.geom;

import math.*;

/**
 * @author David Gronlund
 */
public class Ray {

    public Vector4 origin;
    public Vector4 direction;

    public Ray(Vector4 origin, Vector4 direction) {
        this.origin = origin;
        this.direction = direction;
    }

    public Vector4 calculate(float value) {
        return new Vector4(origin).add(new Vector4(direction).multiply(value));
    }
}
