package math.geom;

import math.*;

/**
 * @author David Gronlund
 */
public class Plane {

    public Vector4 origin;
    public Vector4 normal;

    public Plane(Vector4 origin, Vector4 normal) {
        this.origin = origin;
        this.normal = normal;
    }

    public static float distance(Plane plane, Vector4 point) {
        float D = GameMath.dotProduct(new Vector4(plane.normal).multiply(-1), plane.origin);
        return GameMath.dotProduct(plane.normal, point) + D;
    }

    public static Vector4 intersection(Plane plane, Ray ray) {
        plane.normal.normalise();
        ray.direction.normalise();
        float D = GameMath.dotProduct(new Vector4(plane.normal).multiply(-1), plane.origin);
        float numerator = -(GameMath.dotProduct(plane.normal, ray.origin) + D);
        float denominator = GameMath.dotProduct(plane.normal, ray.direction);
        if (denominator == 0) {
            if (numerator == 0) {
                return new Vector4(Float.POSITIVE_INFINITY);
            } else {
                return new Vector4(Float.NaN);
            }
        }
        float t = numerator / denominator;
        return ray.calculate(t);
    }
}
