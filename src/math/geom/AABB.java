package math.geom;

/**
 * @author David Gronlund
 */
public class AABB {

    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    public float minZ;
    public float maxZ;

    public AABB(float minX, float maxX, float minY, float maxY, float minZ, float maxZ) {
        if (minX > maxX) {
            this.minX = maxX;
            this.maxX = minX;
        } else {
            this.minX = minX;
            this.maxX = maxX;
        }
        if (minY > maxY) {
            this.minY = maxY;
            this.maxY = minY;
        } else {
            this.minY = minY;
            this.maxY = maxY;
        }
        if (minZ > maxZ) {
            this.minZ = maxZ;
            this.maxZ = minZ;
        } else {
            this.minZ = minZ;
            this.maxZ = maxZ;
        }
    }
}
