package math.geom;

import math.*;

/**
 * @author David Gronlund
 */
public class Line {

    public Vector4 point1;
    public Vector4 point2;

    public Line(Vector4 point1, Vector4 point2) {
        this.point1 = point1;
        this.point2 = point2;
    }

    public Vector4 calculate(float value) {
        return new Vector4(point1).multiply(1 - value).add(new Vector4(point2).multiply(value));
    }
}
