package math;

/**
 * @author David Gronlund
 */
public class MatrixStack {

    private Matrix4[] stack;
    private int top = 0;

    public MatrixStack() {
        stack = new Matrix4[10];
    }

    public MatrixStack(Matrix4 first) {
        stack = new Matrix4[10];
        stack[top] = first;
    }

    public Matrix4 read() {
        return stack[top];
    }

    public Matrix4 read(int offset) {
        return stack[top - offset];
    }

    public int stackHeight() {
        return top + 1;
    }

    public void push(Matrix4 matrix) {
        if (stackHeight() == stack.length) {
            Matrix4[] temp = new Matrix4[stack.length + 5];
            System.arraycopy(stack, 0, temp, 0, stack.length);
            this.stack = temp;
        }
        stack[top + 1] = matrix;
        top++;
    }

    public Matrix4 pop() {
        Matrix4 popped = stack[top];
        if (top != 0) {
            stack[top] = null;
            top--;
        }
        return popped;
    }
}
