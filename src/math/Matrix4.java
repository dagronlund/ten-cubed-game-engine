package math;

import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;

/**
 * @author David Gronlund
 */
public class Matrix4 {

    public float[] matrix;

    public Matrix4() {
        matrix = new float[16];
        for (int i = 0; i < 16; i++) {
            matrix[i] = 0;
        }
    }

    public Matrix4(float m00, float m01, float m02, float m03, float m10, float m11, float m12, float m13,
            float m20, float m21, float m22, float m23, float m30, float m31, float m32, float m33) {
        matrix = new float[16];

        matrix[0] = m00;
        matrix[1] = m01;
        matrix[2] = m02;
        matrix[3] = m03;

        matrix[4] = m10;
        matrix[5] = m11;
        matrix[6] = m12;
        matrix[7] = m13;

        matrix[8] = m20;
        matrix[9] = m21;
        matrix[10] = m22;
        matrix[11] = m23;

        matrix[12] = m30;
        matrix[13] = m31;
        matrix[14] = m32;
        matrix[15] = m33;
    }

    public Matrix4(Matrix4 data) {
        matrix = new float[16];
        for (int i = 0; i < 16; i++) {
            matrix[i] = data.matrix[i];
        }
    }

    public Matrix4(FloatBuffer data) {
        matrix = new float[16];
        for (int i = 0; i < 16; i++) {
            matrix[i] = data.get(i);
        }
    }

    public Matrix4(float[] data) {
        matrix = new float[16];
        for (int i = 0; i < 16; i++) {
            matrix[i] = data[i];
        }
    }

    public Matrix4 setIdentity() {
        matrix[0] = 1;
        matrix[1] = 0;
        matrix[2] = 0;
        matrix[3] = 0;

        matrix[4] = 0;
        matrix[5] = 1;
        matrix[6] = 0;
        matrix[7] = 0;

        matrix[8] = 0;
        matrix[9] = 0;
        matrix[10] = 1;
        matrix[11] = 0;

        matrix[12] = 0;
        matrix[13] = 0;
        matrix[14] = 0;
        matrix[15] = 1;
        return this;
    }

    public float get(int column, int row) {
        return GameMath.get(matrix, 4, column, row);
    }

    public Matrix4 set(int column, int row, float value) {
        GameMath.set(matrix, 4, column, row, value);
        return this;
    }

    public Matrix3 getMatrix3() {
        return new Matrix3(matrix[0], matrix[1], matrix[2], matrix[4], matrix[5], matrix[6], matrix[8], matrix[9], matrix[10]);
    }

    public Matrix4 setMatrix3(Matrix3 values) {
        matrix[0] = values.matrix[0];
        matrix[1] = values.matrix[1];
        matrix[2] = values.matrix[2];

        matrix[4] = values.matrix[3];
        matrix[5] = values.matrix[4];
        matrix[6] = values.matrix[5];

        matrix[8] = values.matrix[6];
        matrix[9] = values.matrix[7];
        matrix[10] = values.matrix[8];

        return this;
    }

    public FloatBuffer toBuffer() {
        FloatBuffer temp = BufferUtils.createFloatBuffer(16);
        temp.put(matrix);
        return temp;
    }

    public float[] toArray() {
        float temp[] = new float[16];
        System.arraycopy(matrix, 0, temp, 0, 16);
        return temp;
    }

    public Matrix4 fromBuffer(FloatBuffer buf) {
        buf.get(matrix, 0, 16);
        return this;
    }

    public Matrix4 fromArray(float[] array) {
        System.arraycopy(array, 0, matrix, 0, 16);
        return this;
    }

    public Matrix4 transpose() {
        this.matrix = GameMath.transposeMatrix(matrix, 4);
        return this;
    }

    public Matrix4 adjugate() {
        this.matrix = GameMath.adjugateMatrix(matrix, 4);
        return this;
    }

    public Matrix4 add(float operand) {
        this.matrix = GameMath.add(matrix, operand);
        return this;
    }

    public Matrix4 add(Matrix4 operator) {
        this.matrix = GameMath.add(matrix, operator.matrix);
        return this;
    }

    public Matrix4 multiply(float operand) {
        this.matrix = GameMath.multiply(matrix, operand);
        return this;
    }

    public Matrix4 multiply(Matrix4 operator) {
        this.matrix = GameMath.multiply(matrix, operator.matrix, 4);
        return this;
    }

    public float determinant() {
        return GameMath.determinant(matrix, 4);
    }

    public Matrix4 inverse() {
        this.matrix = GameMath.invert(matrix, 4);
        return this;
    }

    @Override
    public String toString() {
        return "Matrix4: {" + matrix[0] + ", " + matrix[4] + ", " + matrix[8] + ", " + matrix[12] + ",\n"
                + "          " + matrix[1] + ", " + matrix[5] + ", " + matrix[9] + ", " + matrix[13] + ",\n"
                + "          " + matrix[2] + ", " + matrix[6] + ", " + matrix[10] + ", " + matrix[14] + ",\n"
                + "          " + matrix[3] + ", " + matrix[7] + ", " + matrix[11] + ", " + matrix[15] + " }";
    }

    public static Vector4 multiplyByVector(Matrix4 mat, Vector4 vec) {
        return new Vector4().fromArray(GameMath.multiplyVector(mat.matrix, vec.toArray(), 4));
    }

    //Geometry Transforms
    public Matrix4 translate(float x, float y, float z) {
        this.translate(new Vector4(x, y, z));
        return this;
    }

    public Matrix4 translate(Vector4 direction) {
        Matrix4 temp = new Matrix4().setToTranslate(direction);
        this.multiply(temp);
        return this;
    }

    public Matrix4 scale(float x, float y, float z) {
        this.scale(new Vector4(x, y, z));
        return this;
    }

    public Matrix4 scale(Vector4 scalar) {
        Matrix4 temp = new Matrix4().setToScale(scalar);
        this.multiply(temp);
        return this;
    }

    public Matrix4 rotate(float x, float y, float z, float radians) {
        this.rotate(new Vector4(x, y, z), radians);
        return this;
    }

    public Matrix4 rotate(Vector4 a, float radians) {
        Matrix4 temp = new Matrix4().setToRotate(a, radians);
        this.multiply(temp);
        return this;
    }

    public Matrix4 setToTranslate(float x, float y, float z) {
        this.setToTranslate(new Vector4(x, y, z));
        return this;
    }

    public Matrix4 setToTranslate(Vector4 direction) {
        this.setIdentity();
        this.matrix[12] = direction.x;
        this.matrix[13] = direction.y;
        this.matrix[14] = direction.z;
        return this;
    }

    public Matrix4 setToScale(float x, float y, float z) {
        this.setToScale(new Vector4(x, y, z));
        return this;
    }

    public Matrix4 setToScale(Vector4 scalar) {
        this.setIdentity();
        this.matrix[0] = scalar.x;
        this.matrix[5] = scalar.y;
        this.matrix[10] = scalar.z;
        return this;
    }

    public Matrix4 setToRotate(float x, float y, float z, float radians) {
        this.setToRotate(new Vector4(x, y, z), radians);
        return this;
    }

    public Matrix4 setToRotate(Vector4 a, float radians) {
        a = new Vector4(a).normalise();
        float c = GameMath.cos(radians);
        float s = GameMath.sin(radians);
        float t = 1 - c;
        float x = a.x;
        float y = a.y;
        float z = a.z;
        float x2 = a.x * a.x;
        float y2 = a.y * a.y;
        float z2 = a.z * a.z;
        this.setIdentity();

        this.matrix[0] = c + t * x2;
        this.matrix[1] = t * x * y + s * z;
        this.matrix[2] = t * x * z - s * y;

        this.matrix[4] = t * x * y - s * z;
        this.matrix[5] = c + t * y2;
        this.matrix[6] = t * y * z + s * x;

        this.matrix[8] = t * x * z + s * y;
        this.matrix[9] = t * y * z - s * x;
        this.matrix[10] = c + t * z2;

        return this;
    }
}
