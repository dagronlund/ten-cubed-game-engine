uniform mat4 mvpMatrix;
attribute vec4 inVertex;
void main(void){
	gl_Position = mvpMatrix * inVertex;
}